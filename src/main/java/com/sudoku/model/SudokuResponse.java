package com.sudoku.model;

import java.util.ArrayList;
import java.util.List;

public class SudokuResponse {
    List<String> lineIDs = new ArrayList<>();
    List<String> columnIDs = new ArrayList<>();
    List<String> areaIDs = new ArrayList<>();

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public boolean isComplete() {
        return complete;
    }

    boolean complete;


    public void addLine(Integer line){
        lineIDs.add(line.toString());
    }
    public void addColumn(Integer column){
        columnIDs.add(column.toString());
    }
    public void addArea(Integer area){
        areaIDs.add(area.toString());
    }

    @Override
    public String toString() {
        return "SudokuResponse{" +
                "lineIDs=" + lineIDs +
                ", columnIDs=" + columnIDs +
                ", areaIDs=" + areaIDs +
                '}';
    }
}
