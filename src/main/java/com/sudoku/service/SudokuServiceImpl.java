package com.sudoku.service;

import com.sudoku.model.SudokuResponse;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SudokuServiceImpl implements SudokuService {

    @Override
    public boolean checkSequence(String sequence) {
        for (int i = 0; i < sequence.length(); i++) {
            for (int j = i + 1; j < sequence.length(); j++) {
                if (sequence.charAt(i) == sequence.charAt(j)) {
                    return true;
                }
            }
        }
        return false;
    }



    @Override
    public SudokuResponse checkSudokuFromFile(String fileName) {
        List<List<Integer>>  allSudoku = new ArrayList<>();
        SudokuResponse sudokuResponse = new SudokuResponse();
        sudokuResponse.setComplete(true);
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));

            String line;
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                List<String> arrString = Arrays.asList(line.split(";"));
                allSudoku.add(arrString.stream().map(Integer::parseInt).collect(Collectors.toList()));
            }
        } catch (IOException e) { e.printStackTrace(); }
        String[][] sudokuBoard = new String[9][9];
        for(int i=0; i<9; i++){
            for(int j=0; j<9; j++){
                sudokuBoard[i][j] = String.valueOf(allSudoku.get(i).get(j));
                System.out.print(sudokuBoard[i][j]);
            }
            System.out.println();
        }
        //CHECK LINES
        String valuesToCheck = "";
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                valuesToCheck += sudokuBoard[i][j];
            }
            if (checkSequence(valuesToCheck)) {
                sudokuResponse.addLine(i + 1);
                sudokuResponse.setComplete(false);
            }
            valuesToCheck = "";
        }

        //CHECK COLUMNS
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                valuesToCheck += sudokuBoard[j][i];
            }
            if (checkSequence(valuesToCheck)) {
                sudokuResponse.addColumn(i + 1);
                sudokuResponse.setComplete(false);
            }
            valuesToCheck = "";
        }
        //CHECK AREAS
        int areaNumber = 0;
        List<Integer> areaNumberCheched = new ArrayList<>();
        for (int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3) {
                areaNumber++;
                for (int pos = 0; pos < 8; pos++) {
                    for (int pos2 = pos + 1; pos2 < 9; pos2++) {
                        if (sudokuBoard[row + pos % 3][col + pos / 3] == sudokuBoard[row + pos2 % 3][col + pos2 / 3]) {
                            if (!areaNumberCheched.contains(areaNumber)) {
                                areaNumberCheched.add(areaNumber);
                                sudokuResponse.addArea(areaNumber);
                                sudokuResponse.setComplete(false);
                            }
                        }
                    }

                }
            }

        }
        return sudokuResponse;
    }


}
