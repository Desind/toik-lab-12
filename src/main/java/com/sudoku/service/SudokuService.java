package com.sudoku.service;



import com.sudoku.model.SudokuResponse;

public interface SudokuService {
    public boolean checkSequence(String sequence);
    public SudokuResponse checkSudokuFromFile(String filename);
}
