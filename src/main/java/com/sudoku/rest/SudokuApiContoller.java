package com.sudoku.rest;
import com.sudoku.model.SudokuResponse;
import com.sudoku.service.SudokuService;
import com.sudoku.service.SudokuServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class SudokuApiContoller {
    public SudokuApiContoller() {
    }

    SudokuService sudokuService = new SudokuServiceImpl();


    @CrossOrigin
    @PostMapping("/api/sudoku/verify")
    //GETTING CSV FILE CONTENT FROM CLIENT
    public ResponseEntity<String> checkSudoku(@RequestBody String sudokuBoard){
        SudokuResponse sudokuResponse = sudokuService.checkSudokuFromFile("file.csv");
        if(sudokuResponse.isComplete()){
            return ResponseEntity.ok().body("");
        }else{
            return ResponseEntity.badRequest().body(sudokuResponse.toString());
        }
    }
}
